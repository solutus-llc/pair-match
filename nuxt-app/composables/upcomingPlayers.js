export const getUpcomingPlayers = () => {

    const playedMatches = usePlayedMatches();
    const reservedMatches = useReservedMatches();
    const players = usePlayers();

    // 予約中ユーザのIDを抽出
    const reservedPlayerIds = reservedMatches.value.reduce((acc, match) => {
        acc.push(...match.playerIds);
        return acc;
    }, []);

    // 試合中ユーザのIDを抽出
    const playingPlayerIds = playedMatches.value
        .filter(match => !match.finished)
        .reduce((acc, match) => {
            acc.push(...match.playerIds);
            return acc;
        }, []);

    // 休憩中ユーザのIDを抽出
    const pausePlayerIds = players.value
        .filter(player => player.pause)
        .map(player => player.id);

    // 次の試合でプレイする可能性のあるプレイヤーを抽出
    const upcomingPlayers = players.value
        .filter(player =>
            !reservedPlayerIds.includes(player.id) &&
            !playingPlayerIds.includes(player.id) &&
            !pausePlayerIds.includes(player.id)
        )

    return upcomingPlayers;
};

export const getUpcomingMatches = () => {

    const fullCalc = useFullCalc();
    const playedMatches = usePlayedMatches();
    const upcomingPlayers = getUpcomingPlayers();
    const upcomingMatches = [];

    if (upcomingPlayers.length < 4) {
        return upcomingMatches;
    }

    upcomingPlayers.sort((a, b) => {
        const metricsA = getPlayerMetrics(a.id);
        const metricsB = getPlayerMetrics(b.id);

        if (metricsA.playCount !== metricsB.playCount) {
            return metricsA.playCount - metricsB.playCount;
        }
        if (metricsA.latestPlayTime.seconds !== metricsB.latestPlayTime.seconds) {
            return metricsA.latestPlayTime.seconds - metricsB.latestPlayTime.seconds;
        }

        return 0;
    });

    // 最初のプレイヤーの最終プレイ時間を取得
    const firstPlayerLatestTime = getPlayerMetrics(upcomingPlayers[0].id).latestPlayTime.seconds;

    // 最初のプレイヤーと同じ最終プレイ時間のプレイヤーだけをフィルタリング。つまり、キノコとフラワーが相互的に含まれる
    const playerACandidates = upcomingPlayers.filter(player =>
        getPlayerMetrics(player.id).latestPlayTime.seconds === firstPlayerLatestTime
    );

    // playerACandidates の最初のプレイヤーとupcomingPlayersを使ってペアを生成
    const initialPairLeftCandidates = genPairs([playerACandidates[0]], upcomingPlayers);

    // 初回のペア生成が空の場合、playerACandidates 全体を使ってペア生成を試みる
    const pairLeftCandidates = initialPairLeftCandidates.length > 0 ? initialPairLeftCandidates : genPairs(playerACandidates, upcomingPlayers);

    if (pairLeftCandidates.length === 0) {
        return upcomingMatches;
    }

    const pairLeft = pairLeftCandidates[0];

    const opponentPlayerCandidates = upcomingPlayers.filter(player => {
        // pairLeft.playerIds に含まれていないプレイヤーを対象とする
        return !pairLeft.playerIds.includes(player.id);
    })

    // PairRight 候補の生成
    const pairRightCandidates = genPairs(opponentPlayerCandidates, opponentPlayerCandidates);

    if (pairRightCandidates.length === 0) {
        return upcomingMatches;
    }

    const sortedByMetrics = pairRightCandidates.map(pairRightCandidate => {
        
        // let totalArea = 0;
        // let totalRadians = 0;
        // let totalAcBcDistance = 0;
        // pairRightCandidate.playerIds.forEach(playerId => {
        //     const results = calculateAngleWithPlayer(pairLeft, getPlayerById(playerId), playedMatches.value);
        //     totalArea += results.area;
        //     totalRadians += results.thetaCInRadians;
        //     const acBcDistance = results.acDistance + results.bcDistance;
        //     totalAcBcDistance += acBcDistance;
        // });
        // const pairRightCount = getPairCount(pairRightCandidate, playedMatches.value);
        // const avgDistanceSum = [...pairLeft.playerIds, ...pairRightCandidate.playerIds]
        //     .map(id => getPlayerMetrics(id).avgDistance)
        //     .reduce((acc, distance) => acc + distance, 0);

        const playCounts = [...pairRightCandidate.playerIds].map(id => getPlayerMetrics(id).playCount);
        const totalPlayCount = playCounts.reduce((acc, count) => acc + count, 0); // playCounts の合計を計算
        const minPlayCount = Math.min(...playCounts);
        const maxPlayCount = Math.max(...playCounts);

        const evaluationMatch = { playerIds: [...pairLeft.playerIds, ...pairRightCandidate.playerIds] };
        const evaluationMatchLatestTime = getMatchLatestTime(evaluationMatch, playedMatches.value);
        
        const opponentCounts = pairRightCandidate.playerIds.flatMap(playerId => {
            const playerMetrics = getPlayerMetrics(playerId);
            return pairLeft.playerIds.map(pairLeftPlayerId => {
                return playerMetrics.opponents[pairLeftPlayerId] || 0;
            });
        });
        const maxOpponentCount = Math.max(...opponentCounts);
        const minOpponentCount = Math.min(...opponentCounts);
        
        const levelScoreProductDiff = Math.abs(pairLeft.levelScoreProduct - pairRightCandidate.levelScoreProduct);

        return {
            evaluationMatchLatestTime,
            isLargeDiff: levelScoreProductDiff > 2,
            levelScoreProductDiff,
            maxOpponentCount,
            minOpponentCount,
            pairRight: pairRightCandidate,
            totalPlayCount,
            minPlayCount,
            maxPlayCount,
        };

    }).sort((a, b) => {
        // クラス分けの場合に有意なソート
        if (a.isLargeDiff !== b.isLargeDiff) {
            return a.isLargeDiff ? 1 : -1;
        }
        if (a.levelScoreProductDiff !== b.levelScoreProductDiff) {
            return a.levelScoreProductDiff - b.levelScoreProductDiff;
        }

        // 対戦回数をなだらかにするソート
        if (a.minOpponentCount !== b.minOpponentCount) {
            return a.minOpponentCount - b.minOpponentCount;
        }
        if (a.maxOpponentCount !== b.maxOpponentCount) {
            return a.maxOpponentCount - b.maxOpponentCount;
        }
        
        // 全体最適のソート
        // if (a.totalPlayCount !== b.totalPlayCount) {
        //     return a.totalPlayCount - b.totalPlayCount;
        // }
        if (a.minPlayCount !== b.minPlayCount) {
            return a.minPlayCount - b.minPlayCount;
        }
        if (a.maxPlayCount !== b.maxPlayCount) {
            return a.maxPlayCount - b.maxPlayCount;
        }
        
        // 直近での重複を回避する
        // todo: matchcount でもいいかもしれない
        if (a.evaluationMatchLatestTime !== b.evaluationMatchLatestTime) {
            return a.evaluationMatchLatestTime - b.evaluationMatchLatestTime;
        }
        return 0;
    });

    if (fullCalc.value) {

        // ソートされたリストを用いてマッチ候補を生成
        sortedByMetrics.forEach(candidate => {
            upcomingMatches.push({
                pairL: pairLeft,
                pairR: candidate.pairRight,
                playerIds: [
                    ...pairLeft.playerIds,
                    ...candidate.pairRight.playerIds,
                ],
            });
        });

    } else {

        // ソートされたリストから最初の要素だけをフィルタリングして処理
        sortedByMetrics.filter((_, index) => index === 0).forEach(firstCandidate => {
            upcomingMatches.push({
                pairL: pairLeft,
                pairR: firstCandidate.pairRight,
                playerIds: [
                    ...pairLeft.playerIds,
                    ...firstCandidate.pairRight.playerIds,
                ],
            });
        });
    }

    // 予約されたマッチとupcomingMatchesをマージ
    const finalUpcomingMatches = mergeReservedAndUpcomingMatches(upcomingMatches);

    return finalUpcomingMatches;
}

const mergeReservedAndUpcomingMatches = (upcomingMatches = []) => {
    const reservedMatches = useReservedMatches();

    // 予約されたマッチが存在し、かつまだ追加されていない場合、追加する処理
    if (reservedMatches.value !== undefined && reservedMatches.value.length > 0) {
        if (upcomingMatches.length === 0) {
            // upcomingMatchesが空の場合、予約マッチを先頭に追加
            upcomingMatches.unshift(reservedMatches.value[0]);
        } else {
            // 自動生成マッチと予約マッチのプレイヤー優先度を計算
            const prioritiesOfReservedPlayers = reservedMatches.value[0].playerIds.map(playerId =>
                getPlayerLatestTime(getPlayerById(playerId))
            );
            const prioritiesOfFirstMatch = upcomingMatches[0].playerIds.map(playerId =>
                getPlayerLatestTime(getPlayerById(playerId))
            );
            const highestPriorityOfReserved = Math.max(...prioritiesOfReservedPlayers);
            const lowestPriorityOfFirstMatch = Math.min(...prioritiesOfFirstMatch);
            if (highestPriorityOfReserved < lowestPriorityOfFirstMatch) {
                upcomingMatches.unshift(reservedMatches.value[0]);
            }
        }
    }

    return upcomingMatches;
}
