export const getPairTime = (pair) => {
    const playedMatches = usePlayedMatches();

    const pairHash = getPairHash(pair);

    // playedMatchesを逆順にして、最新のマッチから検索を開始する
    const matchingMatch = [...playedMatches.value].reverse().find(match => {
        const [pairL, pairR] = extractMatchPairs(match);
        return getPairHash(pairL) === pairHash || getPairHash(pairR) === pairHash;
    });

    // 該当するマッチが見つかった場合は、そのマッチのlastUpdatedをタイムスタンプとして返す
    // 該当するマッチが見つからなかった場合は、0を返す
    if (matchingMatch !== undefined) {
        return matchingMatch.lastUpdated.seconds
    }

    return 0;
};

/**
 * 試合を開始する
 * @param {Object} match - 試合の情報
 * @param {Array} playedMatches - 過去の試合のリスト
 * @returns {Object} 更新された試合の情報またはnull
 */
export const startMatch = (match) => {
    const playedMatches = usePlayedMatches();

    const matchPlayers = match.playerIds.map(playerId => getPlayerById(playerId));
    const readyPlayers = getReadyPlayers(matchPlayers, playedMatches.value);

    // matchのプレイヤーが全てreadyPlayersに含まれているかどうかを確認
    if (
        match &&
        match.playerIds.every(playerId =>
            readyPlayers.some(readyPlayer => readyPlayer.id === playerId)
        )
    ) {
        const currentDate = getTimestampFromDate();

        match.winnerIds = null;
        match.finished = false;

        // ローカルストレージからハッシュを取得
        const fingerprint = localStorage.getItem('app_fingerprint');

        const updatedMatch = {
            ...match,
            createdAt: currentDate,
            lastUpdated: currentDate,
            fingerprint: fingerprint || null, // ハッシュを含める
        };

        playedMatches.value.push(updatedMatch);

        deleteMatch(match, useReservedMatches().value);

        updatePlayedMatches(usePlayedMatches().value);
        return updatedMatch;
    }

    return null;
};

/**
 * Deletes a match from the provided matches array.
 * 
 * @param {Object} match - The match to delete.
 * @param {Array} matches - The array of matches to remove the match from.
 * @returns {Array} The updated matches array without the deleted match.
 */
export const deleteMatch = (match, matches) => {
    const matchHash = getMatchHash(match);
    const newMatches = matches.filter(m => getMatchHash(m) !== matchHash);

    matches.length = 0; // matches を空にします
    matches.push(...newMatches); // 新しいマッチを追加します

    return matches;
}

export const toggleWinners = (player, match) => {
    const [pairL, pairR] = getMatchPairs(match);
    const winPair =
        player === pairL[0] || player === pairL[1] ? pairL : pairR;

    const winnerIds = winPair.map(p => p.id); // winPair から id を抽出

    if (match.winnerIds && winnerIds.some(id => match.winnerIds.includes(id))) {
        // finish false の場合
        match.winnerIds = null;
    } else {
        // finish true の場合
        match.winnerIds = winnerIds; // id の配列をセット
    }

    return match.winnerIds;
}

export const updateMatchStatus = (match) => {
    const currentDate = getTimestampFromDate()

    match.finished = match.winnerIds !== null;
    match.lastUpdated = currentDate;
}

export const normalizeCount = (count, min, max) => {
    if (max === min) return 1; // すべての値が同じ場合、正規化は1になります
    return (count - min) / (max - min);
};

export const normalizePlayerCount = (count, min, max) => {
    if (max === min) return 1;
    return (count - min) / (max - min);
};

export const getPlayerLatestTime = (player) => {
    const playedMatches = usePlayedMatches();

    for (let i = playedMatches.value.length - 1; i >= 0; i--) {
        const match = playedMatches.value[i];
        if (match.playerIds.includes(player.id)) {
            return match.lastUpdated.seconds
        }
    }
    return 0;
};

export const getPairLatestTime = (pair) => {
    const playedMatches = usePlayedMatches();

    for (let i = playedMatches.value.length - 1; i >= 0; i--) {
        const match = playedMatches.value[i];
        const [pairL, pairR] = extractMatchPairs(match);

        // pair.playerIds のすべての ID が pairL または pairR に含まれているかチェック
        const isPairLMatch = pair.playerIds.every(id => pairL.playerIds.includes(id));
        const isPairRMatch = pair.playerIds.every(id => pairR.playerIds.includes(id));

        if (isPairLMatch || isPairRMatch) {
            return match.lastUpdated.seconds
        }
    }
    return 0;
};

export const getMatchLatestTime = (match) => {
    const playedMatches = usePlayedMatches();
    const matchHash = getMatchHash(match);

    for (let i = playedMatches.value.length - 1; i >= 0; i--) {
        const playedMatch = playedMatches.value[i];
        const playedMatchHash = getMatchHash(playedMatch);

        if (matchHash === playedMatchHash) {
            return playedMatch.lastUpdated.seconds
        }
    }

    return 0;
};

export const genPairs = (basePlayers, sidePlayers) => {
    const pairs = [];
    const existingHashes = new Set();

    for (const basePlayer of basePlayers) {
        const basePlayerLevel = getPlayerLevel(basePlayer.id);
        // const basePlayerCount = getPlayerCount(basePlayer.id);
        // const basePlayerLatestTime = getPlayerMetrics(basePlayer.id).latestPlayTime.seconds;

        for (const sidePlayer of sidePlayers) {
            const sidePlayerLevel = getPlayerLevel(sidePlayer.id);
            // const sidePlayerCount = getPlayerCount(sidePlayer.id);
            // const sidePlayerLatestTime = getPlayerMetrics(sidePlayer.id).latestPlayTime.seconds;

            if (
                basePlayer.id !== sidePlayer.id &&
                !((basePlayerLevel === 1 && sidePlayerLevel === 4) || (basePlayerLevel === 4 && sidePlayerLevel === 1)) &&
                !(basePlayerLevel === 1 && sidePlayerLevel === 1)
            ) {
                const distance = basePlayer.metrics.distances[sidePlayer.id] || 0;
                const pairCount = getPairCount({ playerIds: [basePlayer.id, sidePlayer.id] });
                const time = getPairTime({ playerIds: [basePlayer.id, sidePlayer.id] });

                // 以下、不要だったがおいておく
                // const totalLatestTime = basePlayerLatestTime + sidePlayerLatestTime;
                // const levelScoreSum = basePlayerLevel + sidePlayerLevel;
                // const levelScoreProduct = basePlayerLevel * sidePlayerLevel;
                // const levelScoreDifference = Math.abs(basePlayerLevel - sidePlayerLevel);
                // const playCounts = [basePlayer.id, sidePlayer.id].map(id => getPlayerMetrics(id).playCount);
                // const totalPlayCount = playCounts.reduce((sum, count) => sum + count, 0);
                // const minPlayCount = Math.min(...playCounts);
                // const maxPlayCount = Math.max(...playCounts);

                const pair = {
                    playerIds: [basePlayer.id, sidePlayer.id],
                    distance,
                    pairCount,
                    time,
                    levelScoreProduct: basePlayerLevel * sidePlayerLevel,
                };

                const hash = getPairHash(pair);

                if (!existingHashes.has(hash)) {
                    pairs.push(pair);
                    existingHashes.add(hash);
                }
            }
        }
    }

    pairs.sort((a, b) => {

        if (a.distance !== b.distance) {
            return a.distance - b.distance;
        }
        if (a.pairCount !== b.pairCount) {
            return a.pairCount - b.pairCount;
        }
        if (a.time !== b.time) {
            return a.time - b.time;
        }

        return 0;
    });

    // sidePlayers のhigh属性が3未満の場合はパラーバランスが崩れるのでペア優先度を下げる
    const highPlayerCountInUpcoming = sidePlayers.filter(player => {
        const playerLevel = getPlayerLevel(player.id);
        return playerLevel === 4;
    }).length;

    const isHighPlayerCountLessThanFour = highPlayerCountInUpcoming < 3;

    if (isHighPlayerCountLessThanFour) {
        // "high" グループのプレイヤー数が3未満の場合、levelScoreProduct が16のペアを後にする
        // つまり、競合がいなくなるのでスターペアの優先度をさげる
        pairs.sort((a, b) => {
            // levelScoreProduct が16であるかどうかを確認し、boolean 値に変換（16ならtrue、それ以外はfalse）
            const isALevelScoreProduct16 = a.levelScoreProduct === 16;
            const isBLevelScoreProduct16 = b.levelScoreProduct === 16;

            if (isALevelScoreProduct16 === isBLevelScoreProduct16) {
                return 0;
            }

            if (isALevelScoreProduct16) {
                return 1;
            }

            return -1;
        });
    }

    return pairs
};
