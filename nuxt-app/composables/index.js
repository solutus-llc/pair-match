// index.js
import CryptoJS from "crypto-js";
import FingerprintJS from "@fingerprintjs/fingerprintjs";

export const initializeFingerprint = async () => {
    const fingerprint = useFingerprint();

    if (typeof window !== 'undefined') {
        let storedFingerprint = localStorage.getItem("app_fingerprint");
        if (!storedFingerprint) {
            const fp = await FingerprintJS.load();
            const result = await fp.get();
            storedFingerprint = result.visitorId;
            localStorage.setItem("app_fingerprint", storedFingerprint);
        }
        fingerprint.value = storedFingerprint;
    }
    return fingerprint.value;
};

export const setPlayerHash = (hash) => {
    if (typeof window !== 'undefined') {
        localStorage.setItem('profile_hash', hash);
    }
    usePlayerHash().value = hash;
};

export const getPlayerHashById = (playerId) => {
    const sessionData = useSessionData();

    if (sessionData.value?.entryPlayers) {
        const playerHash = sessionData.value.entryPlayers[playerId - 1];
        return playerHash;
    }

    return null; // プレイヤーが見つからない場合はnullを返す
};

export const getPlayerHash = () => {
    if (typeof window !== 'undefined') {
        const hash = localStorage.getItem('profile_hash');
        if (hash) {
            usePlayerHash().value = hash;
            return hash;
        }
    }
    return null;
};

export const getPlayerId = (hash = '', entryPlayers = []) => {
    if (!entryPlayers) {
        return null;
    }
    const index = entryPlayers.indexOf(hash);
    return index !== -1 ? index + 1 : null;
};

export const setPlayerIdFromProfileHash = async () => {
    const profileHash = getPlayerHash();
    const entryPlayers = useEntryPlayers().value;

    if (profileHash && entryPlayers) {
        const playerId = getPlayerId(profileHash, entryPlayers)
        usePlayerId().value = playerId;
        return playerId;
    }

    return null;
};

export const generateIntID = (value1, value2) => {
    const smallValue = Math.min(value1, value2);
    const largeValue = Math.max(value1, value2);
    const hash =
        ((largeValue + smallValue) * (largeValue + smallValue + 1)) / 2 +
        smallValue;
    return parseInt(hash);
}

export const getMatchHash = (match) => {
    return generateIntID(
        generateIntID(match.playerIds[0], match.playerIds[1]),
        generateIntID(match.playerIds[2], match.playerIds[3])
    );
}

export const getPairHash = (pair) => {
    return generateIntID(pair.playerIds[0], pair.playerIds[1]);
}

export const generatePairs = (playerIds) => {
    const result = [];

    for (let i = 0; i < playerIds.length; i++) {
        const playerIdA = playerIds[i];

        for (let j = i + 1; j < playerIds.length; j++) {
            const playerIdB = playerIds[j];

            const pair = {
                playerIds: [playerIdA, playerIdB],
            };

            const isDuplicate = result.some((existingPair) => {
                return existingPair.playerIds.includes(playerIdA) && existingPair.playerIds.includes(playerIdB);
            });

            if (!isDuplicate) {
                result.push(pair);
            }
        }
    }

    return result;
};

export const generateMatches = (playerIds) => {
    const pairs = generatePairs(playerIds);
    const matches = [];

    for (let i = 0; i < pairs.length; i++) {
        const pairA = pairs[i];

        for (let j = i + 1; j < pairs.length; j++) {
            const pairB = pairs[j];

            // 同じプレイヤーを含む組み合わせはスキップ
            if (pairA.playerIds.some(id => pairB.playerIds.includes(id))) {
                continue;
            }

            const match = {
                playerIds: pairA.playerIds.concat(pairB.playerIds),
                finished: true,
            };

            const isDuplicate = matches.some((existingMatch) => getMatchHash(existingMatch) === getMatchHash(match));

            if (!isDuplicate) {
                matches.push(match);
            }
        }
    }

    return matches;
};

export const getMatchPairs = (match) => {
    const pairL = [getPlayerById(match.playerIds[0]), getPlayerById(match.playerIds[1])];
    const pairR = [getPlayerById(match.playerIds[2]), getPlayerById(match.playerIds[3])];
    return [pairL, pairR];
};

export const extractMatchPairs = (match) => {
    const pairL = { playerIds: [match.playerIds[0], match.playerIds[1]] };
    const pairR = { playerIds: [match.playerIds[2], match.playerIds[3]] };
    return [pairL, pairR];
};

export const getPairCount = (pair) => {
    // pair.playerIds[0]のプレイヤーのメトリクスを取得
    const playerMetrics = getPlayerMetrics(pair.playerIds[0]);

    // pairsプロパティから、pair.playerIds[1]とのペア回数を取得
    const count = playerMetrics.pairs[pair.playerIds[1]] || 0;

    return count;
};

export const getMatchCount = (match, playedMatches) => {
    const count = playedMatches.filter(
        (playedMatch) => getMatchHash(playedMatch) === getMatchHash(match)
    ).length;
    return count;
};

export const getPlayerCount = (playerId) => {
    // 指定されたプレイヤーのメトリクスを取得
    const playerMetrics = getPlayerMetrics(playerId);

    // メトリクスからplayerCountを取得して返す
    return playerMetrics ? playerMetrics.playCount : 0;
};

export const getPlayerRelationshipDistribution = (targetPlayer) => {
    const players = usePlayers();
    const relationshipDistribution = [];

    // 自分自身も含めた全プレイヤーに対してループ
    for (const player of players.value) {
        // 各プレイヤーのメトリクスを取得
        const metrics = calcPlayerMetrics(player.id);

        // 指定されたプレイヤーと各プレイヤーとの関係性メトリクスを配列に追加
        // ここで metrics.distances は指定されたプレイヤー p.id との関係性を示すものとする
        relationshipDistribution.push({
            player: player,
            opponentCount: metrics.opponents[targetPlayer.id] || 0,
            partnerCount: metrics.pairs[targetPlayer.id] || 0,
            relationshipDistance: metrics.distances[targetPlayer.id] || 0,
        });
    }

    return relationshipDistribution;
};

export const calcPlayerMetrics = (playerId) => {
    const playedMatches = usePlayedMatches().value;
    const players = usePlayers().value;

    let playCount = 0;
    let latestPlayTime = { seconds: 0 }; // オブジェクトとして初期化
    const opponents = {};
    const pairs = {};
    const distances = {};

    for (const match of playedMatches) {
        if (match.playerIds.includes(playerId)) {
            playCount += 1;
            // seconds で比較し、より新しい lastUpdated オブジェクトを保持
            if (match.lastUpdated.seconds > latestPlayTime.seconds) {
                latestPlayTime = match.lastUpdated;
            }

            const [pairL, pairR] = getMatchPairs(match);
            let opponentPair, partnerPair;

            if (pairL.some(player => player.id === playerId)) {
                partnerPair = pairL;
                opponentPair = pairR;
            } else {
                partnerPair = pairR;
                opponentPair = pairL;
            }

            partnerPair.forEach(player => {
                if (player.id !== playerId) {
                    pairs[player.id] = (pairs[player.id] || 0) + 1;
                }
            });

            opponentPair.forEach(player => {
                if (player.id !== playerId) {
                    opponents[player.id] = (opponents[player.id] || 0) + 1;
                }
            });
        }
    }

    // 距離の計算：すべてのプレイヤーに対して実行
    players.forEach(player => {
        const opponentCount = opponents[player.id] || 0;
        const pairCount = pairs[player.id] || 0;
        // 自分自身に対する距離計算は、自分自身であれば0、そうでなければ計算
        distances[player.id] = player.id === playerId ? 0 :
            Math.round((opponentCount + (pairCount * Math.sqrt(2))) * 1000) / 1000;
    });

    // distancesからmin, max, avgを計算し、小数点以下第3位まで丸める
    const distanceValues = Object.values(distances).filter(value => value !== 0); // 自分自身は除外
    const minDistance = Math.round(Math.min(...distanceValues) * 1000) / 1000;
    const maxDistance = Math.round(Math.max(...distanceValues) * 1000) / 1000;
    const avgDistance = distanceValues.length > 0 ?
        Math.round((distanceValues.reduce((acc, cur) => acc + cur, 0) / distanceValues.length) * 1000) / 1000 :
        0;

    return {
        playerId,
        playCount,
        latestPlayTime,
        opponents,
        pairs,
        distances,
        minDistance,
        maxDistance,
        avgDistance,
    };
};

export const updatePlayerMetrics = () => {
    const players = usePlayers().value; // すべてのプレイヤーを取得
    const updatedPlayers = []; // 更新されたプレイヤー情報を一時的に格納する配列

    for (const player of players) {
        // 各プレイヤーのメトリクスを計算
        const metrics = calcPlayerMetrics(player.id);
        // 計算されたメトリクスをプレイヤーの metrics プロパティとして設定
        updatedPlayers.push({ ...player, metrics });
    }

    // usePlayers().value を直接更新するのではなく、更新されたプレイヤーリストを使って状態を更新
    usePlayers().value = updatedPlayers;
};

export const getPlayerMetrics = (playerId) => {
    const players = usePlayers().value; // すべてのプレイヤーのリストを取得
    const player = players.find(player => player.id === playerId); // 指定されたIDのプレイヤーを検索
    return player ? player.metrics : null; // プレイヤーが見つかった場合はそのmetricsを返し、見つからない場合はnullを返す
};

export const usePath = () => {
    const route = useRoute();
    const path = ref(route.path);

    const isHomePage = computed(() => path.value === '/');

    return {
        path,
        isHomePage,
    };
}

export const getPairLastUpdatedTime = (pair, playedMatches) => {
    // pair の hash 値を取得
    const pairHash = getPairHash(pair);

    // playedMatches から pair を含む試合を探す
    const matchedMatches = playedMatches.filter((match) => {
        const [pairL, pairR] = getMatchPairs(match);
        return getPairHash(pairL) === pairHash || getPairHash(pairR) === pairHash;
    });

    // matchedMatches の中で最も新しい lastUpdated を取得
    let latestLastUpdated = 0;
    matchedMatches.forEach((match) => {
        if (match.lastUpdated.seconds > latestLastUpdated) {
            latestLastUpdated = match.lastUpdated.seconds;
        }
    });

    return latestLastUpdated;
};

// 階乗を計算する関数
export function factorial(n) {
    let result = 1;
    for (let i = 1; i <= n; i++) {
        result *= i;
    }
    return result;
}

// 組み合わせを計算する関数
export function combination(n, r) {
    return factorial(n) / (factorial(r) * factorial(n - r));
}

export function permutation(n, r) {
    return factorial(n) / factorial(n - r);
}

export const getActivePlayers = (playedMatches) => {
    // 完了していない試合を抽出
    const activeMatches = playedMatches.filter(match => !match.finished);

    // それらの試合からプレイヤーを抽出してリスト化
    let activePlayers = [];
    activeMatches.forEach(match => {
        const players = match.playerIds.map(playerId => getPlayerById(playerId));
        activePlayers.push(...players);
    });

    // アクティブなプレイヤーのリストを返す
    return activePlayers;
}

/**
 * 利用可能なプレイヤーのリストを取得する
 * @param {Array} players - 全プレイヤーのリスト
 * @param {Array} playedMatches - 過去の試合のリスト
 * @returns {Array} 利用可能なプレイヤーのリスト
 */
export const getReadyPlayers = (players, playedMatches) => {
    // アクティブなプレイヤーのリストを取得
    const activePlayers = getActivePlayers(playedMatches);

    // pauseがtrueでない、かつアクティブでないプレイヤーだけを取得
    const availablePlayers = players.filter(player => {
        return !player.pause && !activePlayers.includes(player);
    });

    return availablePlayers;
}

export const getAllPossiblePairs = (candidatePlayers, players) => {

    const pairs = [];
    const existingHashes = new Set(); // これまでに作成されたペアのハッシュ値を保持。

    for (const candidatePlayer of candidatePlayers) {
        for (const player of players) {
            if (candidatePlayer.id !== player.id) {
                const hash = getPairHash({ playerIds: [candidatePlayer.id, player.id] });

                if (!existingHashes.has(hash)) {
                    pairs.push({
                        playerIds: [candidatePlayer.id, player.id],
                    });
                    existingHashes.add(hash); // 作成されたペアのハッシュをセットに追加
                }
            }
        }
    }

    const playedMatches = usePlayedMatches().value;

    // 第一ソートは getPairCount、第二ソートは getPairTime で昇順にソート
    pairs.sort((a, b) => {
        const countA = getPairCount(a, playedMatches);
        const countB = getPairCount(b, playedMatches);

        const priorityA = getPairTime(a);
        const priorityB = getPairTime(b);

        if (countA !== countB) {
            return countA - countB;
        }

        return priorityA - priorityB;
    });

    return pairs;
};

// 指定したプレイヤーが特定のプレイヤーグループの中で何回プレイしたかを計算する関数
export const getOpponentCount = (basePlayerId, opponentPlayerId) => {
    // 基準プレイヤーのメトリクスを取得
    const basePlayerMetrics = getPlayerMetrics(basePlayerId);

    // opponents オブジェクトから対戦相手との対戦回数を取得
    const count = basePlayerMetrics.opponents[opponentPlayerId] || 0;

    return count;
};

export const calculateAngleWithPlayer = (pair, opponentPlayer, playedMatches) => {
    const abDistance = (getPairCount(pair, playedMatches) + 1) * Math.sqrt(2) + getOpponentCount(pair.playerIds[0], pair.playerIds[1], playedMatches);

    const acDistance = (getPairCount({ playerIds: [pair.playerIds[0], opponentPlayer.id] }, playedMatches) + 1) * Math.sqrt(2) + (getOpponentCount(pair.playerIds[0], opponentPlayer.id, playedMatches) + 1);

    const bcDistance = (getPairCount({ playerIds: [pair.playerIds[1], opponentPlayer.id] }, playedMatches) + 1) * Math.sqrt(2) + (getOpponentCount(pair.playerIds[1], opponentPlayer.id, playedMatches) + 1);

    // ac と bc の差分の絶対値を計算
    const acBcDifference = Math.abs(acDistance - bcDistance);

    // 三角形の成立条件を満たすかチェック
    if (abDistance + acDistance <= bcDistance ||
        abDistance + bcDistance <= acDistance ||
        acDistance + bcDistance <= abDistance) {
        // 三角形が成立しない場合、エラー値を返す
        return { error: "Invalid triangle", thetaCInRadians: Math.PI, area: 0, abDistance, acDistance, bcDistance, acBcDifference };
    }

    // thetaC が大きいほど次の対戦相手としての確率が高い
    const thetaCInRadians = parseFloat(Math.acos((acDistance ** 2 + bcDistance ** 2 - abDistance ** 2) / (2 * acDistance * bcDistance)).toFixed(3));

    // ヘロンの公式を使用して三角形の面積を計算
    const s = (abDistance + acDistance + bcDistance) / 2;
    const area = Math.sqrt(s * (s - abDistance) * (s - acDistance) * (s - bcDistance));

    return { thetaCInRadians, area, abDistance, acDistance, bcDistance, acBcDifference };
};

export const addGuestPlayer = async (opts = { group: 'mid', displayName: 'ゲスト' }) => {
    const sessionId = useSessionId();
    const randomBytes = CryptoJS.lib.WordArray.random(64);

    setPlayerHash(null);

    // 生成したバイトを16進数文字列に変換
    const guestHash = randomBytes.toString(CryptoJS.enc.Hex);

    // await setProfileData(guestHash, opts);
    await addToEntryPlayers(sessionId.value, guestHash);

    return guestHash;
};

// プレイヤーIDを受け取り、そのプレイヤーが属するグループ名を返すメソッド
export const getPlayerGroup = (playerId) => {
    const players = usePlayers().value;
    const sessionData = useSessionData();

    const player = players.find(player => player.id === playerId);

    if (player && sessionData.value) {
        const playerHash = player.hash;
        const playerState = sessionData.value.playersState[playerHash];

        if (playerState && playerState.group) {
            return playerState.group.toLowerCase();
        }
    }

    // プレイヤーがどのグループにも属していない場合は、nullを返す
    return null;
};

export const getPlayerById = (playerId) => {
    const players = usePlayers();
    const foundPlayer = players.value.find(player => player.id === Number(playerId));
    return foundPlayer !== undefined ? foundPlayer : null;
};

export const calcPairDistance = (pair) => {

    const playedMatches = usePlayedMatches().value;

    const pairDistance = getOpponentCount(pair.playerIds[0], pair.playerIds[1], playedMatches) + (getPairCount(pair, playedMatches) * Math.sqrt(2));

    return pairDistance;
};

export const getPlayerLevel = (playerId) => {
    const playerGroup = getPlayerGroup(playerId);

    switch (playerGroup) {
        case 'low':
            return 1;
        case 'mid':
            return 2;
        case 'high':
            return 4;
        default:
            return 0; // プレイヤーがどのグループにも属していない場合
    }
};

export const mappingEntryPlayer = async (entryPlayers) => {
    const sessionData = useSessionData();

    if (sessionData.value && Array.isArray(entryPlayers)) {
        const players = usePlayers();

        if (entryPlayers && entryPlayers.length > 0) {
            for (let i = 0; i < entryPlayers.length; i++) {
                const playerId = i + 1;
                const playerHash = entryPlayers[i];
                const playerState = sessionData.value.playersState[playerHash];

                if (playerHash && playerState) {
                    players.value = sessionData.value.players;
                    const existingPlayer = sessionData.value?.players.find(
                        (player) => player.hash === playerHash,
                    );

                    if (!existingPlayer) {
                        // 既存プレイヤーがいない場合のみ新しいプレイヤーを追加
                        const newPlayer = {
                            id: playerId,
                            hash: playerHash,
                            pause: false,
                            metrics: {
                                playerId: playerId,
                                playCount: 0,
                                latestPlayTime: getTimestampFromDate(),
                                opponents: {},
                                pairs: {},
                                distances: {},
                            },
                            group: playerState.group || "mid",
                        };
                        players.value.push(newPlayer);
                    }
                }
            }
        }
    }
};

// ハッシュを生成する関数
export const generateHash = (gender, phoneLast4, fingerprint) => {
    const data = `${gender}-${phoneLast4}-${fingerprint}`;
    return CryptoJS.SHA256(data).toString(CryptoJS.enc.Hex);
};

export const updatePlayedMatches = async (matches = []) => {

    const sessionId = useSessionId();
    const masterMode = useMasterMode();

    try {
        await updateDocument(
            sessionId.value,
            { playedMatches: matches },
            masterMode.value,
        );
    } catch (error) {
        console.error("Error deleting match and updating document:", error);
    }
}
