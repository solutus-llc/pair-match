// states.js

export const usePlayers = () => {
    return useState('players', () => [])
}

// あるセッション内だけのプレイヤー状態を管理する(毎回リセットされる)
export const usePlayersState = () => {
    return useState('playersState', () => { })
}

export const usePlayerHash = () => {
    return useState('playerHash', () => null)
}

export const useEntryPlayers = () => {
    return useState('entryPlayers', () => [])
}

export const usePlayerId = () => {
    return useState('playerId', () => 0)
}

export const usePlayedMatches = () => {
    return useState('playedMatches', () => [])
}

export const useUpcomingMatches = () => {
    return useState('upcomingMatches', () => [])
}

export const useEntryCount = () => {
    return useState('entryCount', () => 21)
}

export const useCourts = () => {
    return useState('courtsNum', () => 1)
}

export const useSessionId = () => {
    return useState('sessionId', () => 'global')
}

export const useSessionData = () => {
    return useState('sessionData', () => null)
}

export const useSessionMeta = () => {
    return useState('sessionMeta', () => null)
}

export const isSessionStarted = () => {
    const playedMatches = usePlayedMatches().value;
    return playedMatches.length > 0;
}

export const useReservedMatches = () => {
    return useState('reservedMatches', () => [])
}

export const useDebugMode = () => {
    return useState('debugMode', () => false)
}

export const useFullCalc = () => {
    return useState('fullCalc', () => false)
}

export const useMasterMode = () => {
    return useState('masterMode', () => false)
}

export const useFingerprint = () => {
    return useState('fingerprint', () => null);
};

export const usePlayerModal = () => {
    const playerModal = useState('playerModal', () => ({}));

    const showPlayerModal = async (hash = null) => {
        if (playerModal.value?.updateIsOpen && hash) {
            setPlayerHash(hash);
            const playersState = usePlayersState();
            const playerState = playersState.value[hash];
            if (playerState) {
                playerModal.value.setFormData({ ...playerState, hash });
            }
        }

        if (playerModal.value?.updateIsOpen) {
            playerModal.value.updateIsOpen(true);
        } else {
            console.warn('playerModalが初期化されていないか、updateIsOpenが関数ではありません');
        }
    };

    const hidePlayerModal = async () => {
        if (playerModal.value?.updateIsOpen) {
            setPlayerHash(null);
            playerModal.value.setFormData(null);
            playerModal.value.updateIsOpen(false);
        } else {
            console.warn('playerModalが初期化されていないか、updateIsOpenが関数ではありません');
        }
    };

    return {
        playerModal,
        showPlayerModal,
        hidePlayerModal,
    };
};

export const useGroups = () => {
    return useState('groups', () => ({
        low: [],
        mid: [],
        high: []
    }));
}
