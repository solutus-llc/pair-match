// from https://console.firebase.google.com/project/ppdd-8ce44/overview?hl=ja

// Import the functions you need from the SDKs you need
import {
    collection,
    doc,
    getFirestore,
    onSnapshot,
    updateDoc,
    limit,
    orderBy,
    serverTimestamp,
    Timestamp,
    setDoc,
    getDoc,
    arrayUnion,
    arrayRemove,
    deleteField,
    connectFirestoreEmulator,
} from "firebase/firestore";
import { initializeApp } from "firebase/app";
import { useRuntimeConfig } from '#app';

export default function useFireState() {
    // TODO: Add SDKs for Firebase products that you want to use
    // https://firebase.google.com/docs/web/setup#available-libraries

    // Your web app's Firebase configuration
    // For Firebase JS SDK v7.20.0 and later, measurementId is optional
    const firebaseConfig = {
        apiKey: "AIzaSyAyr3DVDsTBisCOzfs491RluGW92D9AeBc",
        authDomain: "ppdd-8ce44.firebaseapp.com",
        projectId: "ppdd-8ce44",
        storageBucket: "ppdd-8ce44.appspot.com",
        messagingSenderId: "741259670782",
        appId: "1:741259670782:web:fb3c41ddeaa5ebb342e1af",
        measurementId: "G-YKK6ZLZ9X1"
    };

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);

    const config = useRuntimeConfig();

    const db = getFirestore(app); // Initialize Firestore with app

    // Firestore emulator connection logic
    if (config.public.FIRESTORE_EMULATOR_HOST && config.public.FIRESTORE_EMULATOR_HOST.includes('localhost') && !db._settingsFrozen) {
        const [host, port] = config.public.FIRESTORE_EMULATOR_HOST.split(':');
        connectFirestoreEmulator(db, host, parseInt(port, 10)); // Set emulator before using db
    }

    return db;
}

let unsubscribe = null;

export const getSessionData = async (sessionId) => {
    const db = useFireState()
    const sessionDocRef = doc(db, "sessions", sessionId);
    const sessionDoc = await getDoc(sessionDocRef);

    try {
        if (!sessionDoc.exists()) {
            throw new Error("Session does not exist.");
        }
        return sessionDoc.data();
    } catch (error) {
        console.error("Error writing data: ", error);
    }

    return sessionDoc.exists();
};

// Setup Firestore subscription
export const setupSubscription = async (sessionId = '') => {
    const db = useFireState()
    const sessionData = useSessionData();
    const sessionMeta = useSessionMeta();

    // Check if sessionId is provided and if reactive variables have values
    if (!sessionId) {
        return null;
    }

    const docRef = doc(db, "sessions", sessionId);
    const docSnap = await getDoc(docRef);

    if (!docSnap.exists()) {
        console.log("Document does not exist!");
        return null;
    }

    // 既存の購読があれば解除する
    if (unsubscribe) {
        unsubscribe();
    }

    // 新しいドキュメントに対して購読を開始する
    unsubscribe = onSnapshot(docRef, (doc) => {
        sessionData.value = doc.data();
        sessionMeta.value = doc.metadata;
    });

    return unsubscribe; // 購読解除関数を返す
};

export const clearSubscription = () => {
    if (unsubscribe) {
        unsubscribe();
    }
};

export const updateDocument = async (sessionId = null, data = {}, isMasterMode = false) => {
    const db = useFireState();
    const sessionMeta = useSessionMeta();

    // isMasterMode が false、または sessionId が null の場合、更新をスキップします。
    if (!isMasterMode || sessionId === null) {
        console.log("Update skipped: Invalid sessionId or empty data.");
        return;
    }

    // document reference の作成
    const docRef = doc(db, "sessions", sessionId);
    const docSnap = await getDoc(docRef);

    // 指定された sessionId のドキュメントが存在しない場合、処理を中断します。
    if (!docSnap.exists()) {
        console.log(`Update skipped: Document with sessionId "${sessionId}" does not exist.`);
        return;
    }

    try {
        if (data === null) {
            // data が null の場合、すべてのフィールドを削除
            const existingData = docSnap.data();
            const fieldsToDelete = {};

            Object.keys(existingData).forEach((field) => {
                fieldsToDelete[field] = deleteField();
            });

            await updateDoc(docRef, fieldsToDelete);
            console.log("All fields successfully deleted.");
        } else {
            // 通常のドキュメント更新
            await updateDoc(docRef, data);
            console.log("Document successfully updated.");
        }
    } catch (error) {
        console.error("Error updating document:", error);
    }
};

export const updatePlayerState = async (playerHash = null, playerState = {}) => {
    const db = useFireState()

    // 引数のチェック
    if (!playerHash || Object.keys(playerState).length === 0) {
        console.error("Invalid playerHash or pState provided");
        return;
    }

    try {
        const sessionId = useSessionId().value;
        const docRef = doc(db, "sessions", sessionId);

        // Firestoreのドキュメントを更新
        await updateDoc(docRef, {
            [`playersState.${playerHash}`]: playerState
        });

        console.log(`Player state for ${playerHash} successfully updated.`);
    } catch (error) {
        console.error("Error updating player state:", error);
    }
};

export const createHistory = async () => {
    const db = useFireState()
    const collectionRef = collection(db, "histories");
    const newHistoryRef = doc(collectionRef);
    const sessionId = useSessionId();

    try {
        const data = await getSessionData(sessionId.value);
        if (!data) {
            console.log("No session data available to add to history.");
            return;
        }

        const dataWithTimestamp = {
            ...data,
            timestamp: serverTimestamp(),
            sessionId: sessionId.value
        };

        await setDoc(newHistoryRef, dataWithTimestamp);
        console.log("History created successfully");
    } catch (error) {
        console.error("Error creating history:", error);
    }
};

// Set profile data to Firestore
export const setProfileData = async (hash, data) => {
    const db = useFireState()
    try {
        // Convert reactive object to plain JavaScript object
        const plainData = JSON.parse(JSON.stringify(data));

        await setDoc(doc(db, "profiles", hash), plainData);
        console.log("Document successfully written.");
    } catch (error) {
        console.error("Error writing document:", error);
    }
};

// Get profile data from Firestore
export const getProfileData = async (hash) => {
    const db = useFireState()

    if (hash.startsWith('class_')) {
        const parts = hash.split('_');
        if (parts.length >= 2) {
            const group = parts[1];
            return { playerGroup: group, group: group };
        }
    }

    const profileDocRef = doc(db, "profiles", hash);
    const profileDoc = await getDoc(profileDocRef);

    try {
        if (!profileDoc.exists()) {
            throw new Error("Profile does not exist.");
        }
        return profileDoc.data();
    } catch (error) {
        console.error("Error getting profile data: ", error);
        return null;
    }
};

// Get server timestamp
export const getServerTimestamp = () => {
    return serverTimestamp();
};

export const getTimestampFromDate = (date = null) => {
    if (date === null) {
        return Timestamp.now();
    }
    return Timestamp.fromDate(date);
};

export const resetAllData = async (data = {}) => {
    const masterMode = useMasterMode();
    const sessionId = useSessionId();

    if (Object.keys(data).length === 0) {
        data = {
            playedMatches: [],
            reservedMatches: [],
            players: [],
            playersState: {},
            entryPlayers: [],
        };
    }

    const updateData = {
        ...data,
        resetTimestamp: getTimestampFromDate(),
    };

    try {
        // Update Firestore document with the specified fields
        updateDocument(
            sessionId.value,
            updateData,
            masterMode.value,
        );
        console.log("Document fields successfully reset.");
    } catch (e) {
        console.error("Error updating document: ", e);
    }
};

// Add player hash to entryPlayers array
export const addToEntryPlayers = async (sessionId, hash) => {
    const db = useFireState()
    if (!sessionId || !hash) {
        console.error("Invalid sessionId or hash provided");
        return;
    }

    const docRef = doc(db, "sessions", sessionId);

    try {
        await updateDoc(docRef, {
            entryPlayers: arrayUnion(hash) // ドキュメント参照を arrayUnion する
        });
        console.log(`Player ${hash} successfully added to entryPlayers`);
    } catch (error) {
        console.error("Error adding player to entryPlayers:", error);
    }
};
