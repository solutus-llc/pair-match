
export default {
    content: ["./**/*.{html,js.vue}"],
    theme: {
        extend: {
            backgroundImage: {
                'low': "url('/images/mushroom-fill.png')",
                'mid': "url('/images/flower-fill.png')",
                'high': "url('/images/star-fill.png')",
            },
            boxShadow: {
                'mario': '1px 4px 0px 0px rgba(0, 0, 0, 1)',
                'big-mario': '3px 11px 0px 0px rgba(0, 0, 0, 1)',
            },
            aspectRatio: {
                auto: 'auto',
                square: '1 / 1',
                video: '16 / 9'
            }
        }
    },
    safelist: [
        {
            pattern: /(.*)-(low|mid|high)/,
            variants: ['before'],

        },
    ],
    plugins: [],
}
