// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: [
    'bootstrap/dist/css/bootstrap.css',
    "bootstrap-icons/font/bootstrap-icons.css",
    "animate.css/animate.css",
  ],
   modules: [
     '@vueuse/nuxt',
     '@nuxt/ui',
     'nuxt-gtag',
  ],
  gtag: {
    id: 'G-YKK6ZLZ9X1',
  },
  ui: {
    global: true,
  },
  colorMode: {
    preference: 'light'
  },
  runtimeConfig: {
    public: {
      FIRESTORE_EMULATOR_HOST: process.env.FIRESTORE_EMULATOR_HOST,
    }
  }
})

